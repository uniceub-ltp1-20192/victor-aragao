from Entidades.pessoa import Pessoa

class Mineiro(Pessoa):
    def __init__(self,tipoDeCabelo="enrolado"):
        super().__init__()
        self._tipoDeCabelo = tipoDeCabelo

    @property
    def tipoDeCabelo(self):
        return self._tipoDeCabelo

    @tipoDeCabelo.setter
    def tipoDeCabelo(self,tipoDeCabelo):
        self._tipoDeCabelo = tipoDeCabelo

    def sotaque():
        print('Porrrta')

    def cozinhar():
        print('Pão de quijo')

    def __str__(self):
        return '''\033[1;33m
        Identificador:  {}
        Nome:           {}
        Cpf:            {}
        Peso:           {}
        Tamanho:        {}
        Tipo de cabelo: {}\033[m'''.format(self.identificador,self.nome.title(),self.cpf,self.peso,self.tamanho,self.tipoDeCabelo.title())
