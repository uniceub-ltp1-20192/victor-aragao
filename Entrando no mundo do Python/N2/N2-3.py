#valor da variavel alterado de 300 para 500
cebolas = 500
#valor da variavel alterado de 120 para 180
cebolas_na_caixa = 180
#valor da variavel alterado de 5 para 10
espaco_caixa = 10
#valor da variavel alterado de 60 para 80
caixas = 80

cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos,", caixas_vazias, "caixas vazias")
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")