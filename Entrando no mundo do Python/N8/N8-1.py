lugares = ['italia', 'roma', 'estados unidos', 'islandia', 'belgica']
print("Lista original: ")
print (lugares)
print("\nLista em ordem alfabetica: ")
print(sorted(lugares))
print("\nLista original: ")
print(lugares)
print("\nOrdem alfabetica inversa: ")
print(sorted(lugares , reverse=True))
print("\nLista original: ")
print(lugares)
print("\n Lista invertida: ")
lugares.reverse()
print(lugares)
print("\nLista invertida novamente para voltar ao normal: ")
lugares.reverse()
print(lugares)
print("\nLista em ordem alfabetica: ")
lugares.sort()
print(lugares)
print("\nLista em ordem alfabetica inversa: ")
lugares.sort(reverse=True)
print(lugares)